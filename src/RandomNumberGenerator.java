import java.util.Random;

/**
 * Class with one static method that returns a random number
 * from 0 to the given value
 */
final class RandomNumberGenerator {

    /**
     * Return a random value between 0 and max
     *
     * @param max max value to return
     * @return int
     */
    static int getRandomNumber(int max) {
        Random random = new Random();
        return random.nextInt(max + 1);
    }

}
