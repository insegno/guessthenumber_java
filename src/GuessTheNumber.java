import java.util.Scanner;

/**
 *
 */
public class GuessTheNumber {

    public static void main(String[] argc) {

        Scanner scanner = new Scanner(System.in);
        int numberToGuess = RandomNumberGenerator.getRandomNumber(100);
        int numberOfTries = 0;
        int guess = -1;
        boolean guessed = false;

        // cycle 'till number is guessed or tries is higher of 5
        for (int i = 0; i < 5; i++) {

            // if guessed exit
            if (guessed) {
                System.out.println("You won in " + numberOfTries + " with the number: " + guess);
                return;
            }

            // increment tries
            numberOfTries++;

            // read user guess
            System.out.print("Your " + numberOfTries + " guess: ");
            guess = scanner.nextInt();

            // check guess with solution
            if (guess > numberToGuess) System.out.println("Too High!");
            else if (guess < numberToGuess) System.out.println("Too Low!");
            else guessed = true;

        }

        // you didn't guess the number
        System.out.println("The number was: " + numberToGuess);


    }

}
